package net.sgzdev.cmotevlc;

import java.util.Map;
import java.util.Set;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 *
 * @author carlos
 */
public class ServListAdapter extends ArrayAdapter<VLCController>
{
    private Context m_ctx;
    private int m_rowLayout;

    /**
     * ctor
     *
     * @param c       context
     * @param layout layout
     */
    public ServListAdapter(Context c, int layout)
    {
        super(c, layout);

        m_ctx = c;
        m_rowLayout = layout;

        loadServerList();
    }

    /**
     * Load server IPs
     */
    final protected void loadServerList()
    {
        SharedPreferences prefs = m_ctx.getSharedPreferences(MoteActivity.IP_CONF_FILE, 0);
        Map<String, ?> ips = prefs.getAll();

        for(Map.Entry<String, ?> e : (Set<Map.Entry<String, ?>>)ips.entrySet())
        {
            String ipname = e.getKey();
            String ip[] = ipname.split(":");

            add(new VLCController(ip[0], Integer.valueOf(ip[1])));
        }
    }


    /**
     * Displays data on adapter
     *
     * @param position    data position
     * @param convertView layout view
     * @param parent      parent view
     *
     * @return View
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = View.inflate(getContext(), m_rowLayout, null);
        }

        View v = convertView;
        VLCController data = getItem(position);
        TextView txt = (TextView)v.findViewById(R.id.serv_title);
        txt.setText(data.getIp(true));

        if(data == MoteActivity.controller)
        {
            ImageView icon = (ImageView)v.findViewById(R.id.serv_icon);
            icon.setBackgroundResource(R.drawable.link_16_w);
        }

        return v;
    }
}
