package net.sgzdev.cmotevlc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MoteActivity extends Activity
{
    public static String IP_CONF_FILE = "iplist";
    public static VLCController controller = null;

    private SharedPreferences m_prefs;
    private ServListAdapter m_servListAdapter;

    private TextView m_volume_txt;
    private SeekBar m_volume_sb;

    private ImageButton m_play_btn;
    private ImageButton m_prev_btn;
    private ImageButton m_next_btn;

    private ImageButton m_fs_btn;
    private ImageButton m_serv_btn;
    private ImageButton m_browse_btn;
    private ImageButton m_prefs_btn;
    private ImageButton m_help_btn;

    private TextView m_title_txt;
    private TextView m_seekpos_txt;
    private TextView m_seeklen_txt;
    private SeekBar m_seek_sb;
    private boolean m_seek_lock = false;
    private boolean m_volume_lock = false;

    /**
     * Auto update, response handler
     */
    final private Handler m_timerHdl = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            // display connection status on server list button.
            if(0 == msg.arg1)
            {
                m_serv_btn.setImageResource( R.drawable.link_g );

                String name = controller.getFilename();
                m_title_txt.setText( name );

                if( ! m_volume_lock )
                {
                    m_volume_sb.setProgress( controller.getVolumePercent() );
                }

                updateUiSeekbar();
                updatePlayBtn();
            }
            else if( 1 == msg.arg1 )
            {
                m_serv_btn.setImageResource( R.drawable.link_r );
            }
            else
            {
                m_serv_btn.setImageResource( R.drawable.link_w );
            }

            startUpdateTimer();
        }
    };

    /**
     * Run auto update
     */
    private Runnable m_timerUpd = new Runnable()
    {
        public void run()
        {
            if( null == controller )
            {
                startUpdateTimer();
                return;
            }

            controller.command( "update", m_timerHdl );
        }
    };

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.main );
        setResult( RESULT_CANCELED );

        m_prefs = PreferenceManager.getDefaultSharedPreferences( this );

        m_servListAdapter = new ServListAdapter( this, R.layout.popup_item );

        initControls();
    }

    /**
     * Resuming activity
     */
    @Override
    protected void onResume()
    {
        super.onResume();

        // disable screen off
        Boolean scrnolock = m_prefs.getBoolean( "screen_nolock", false );
        if( scrnolock )
        {
            this.getWindow().addFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );
        }
        else
        {
            this.getWindow().clearFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );
        }

        startUpdateTimer( 1 );
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        stopUpdateTimer();
    }

    @Override
    public boolean dispatchKeyEvent( KeyEvent event )
    {
        int action = event.getAction();
        int keyCode = event.getKeyCode();

        int volume = m_volume_sb.getProgress();

        switch( keyCode )
        {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if( action == KeyEvent.ACTION_UP )
                {
                    volume += 5;
                    if( volume > 100 )
                    {
                        volume = 100;
                    }

                    Integer vol = controller.getVolumeFromPercent( volume );
                    controller.command( "volume", new Object[] { vol } );
                }

                return true;

            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if( action == KeyEvent.ACTION_DOWN )
                {
                    volume -= 5;
                    if( 0 > volume )
                    {
                        volume = 0;
                    }

                    Integer vol = controller.getVolumeFromPercent( volume );
                    controller.command( "volume", new Object[] { vol } );
                }

                return true;

            default:
                return super.dispatchKeyEvent( event );
        }
    }

    /**
     * Init activity controls
     */
    protected void initControls()
    {
                // Volume bar
        m_volume_txt = ( TextView ) findViewById( R.id.volume_txt );
        m_volume_sb = ( SeekBar ) findViewById( R.id.volume_bar );
        m_volume_sb.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener()
        {
            public void onProgressChanged( SeekBar obj, int progress, boolean fromuser )
            {
                m_volume_txt.setText( String.valueOf( progress ) + "%" );
            }

            public void onStartTrackingTouch( SeekBar obj )
            {
                m_volume_lock = true;
            }

            public void onStopTrackingTouch( SeekBar obj )
            {
                m_volume_lock = false;

                try
                {
                    Integer vol = controller.getVolumeFromPercent( m_volume_sb.getProgress() );
                    controller.command( "volume", new Object[] { vol } );
                }
                catch( Exception e )
                {
                    Toast.makeText( MoteActivity.this, R.string.commanderror, Toast.LENGTH_SHORT ).show();
                }
            }
        } );

        m_fs_btn = ( ImageButton )findViewById( R.id.togglefs_btn );
        m_fs_btn.setOnClickListener( new View.OnClickListener()
        {
            public void onClick( View v )
            {
                try
                {
                    controller.command( "fullscreen" );
                }
                catch( Exception e )
                {
                    Toast.makeText( MoteActivity.this, R.string.commanderror, Toast.LENGTH_SHORT ).show();
                }
            }
        });

            // Play Btn
        m_play_btn = ( ImageButton ) findViewById( R.id.play_btn );
        m_play_btn.setOnClickListener( new View.OnClickListener()
        {
            public void onClick( View arg0 )
            {
                try
                {
                    controller.command( "play" );
                }
                catch( Exception e )
                {
                    Toast.makeText( MoteActivity.this, R.string.commanderror, Toast.LENGTH_SHORT ).show();
                }
            }
        });

        // Previous file
        m_prev_btn = ( ImageButton ) findViewById( R.id.prev_btn );
        m_prev_btn.setOnClickListener( new View.OnClickListener()
        {
            public void onClick( View arg0 )
            {
                try
                {
                    controller.command( "prev");
                }
                catch( Exception e )
                {
                    Toast.makeText( MoteActivity.this, R.string.commanderror, Toast.LENGTH_SHORT ).show();
                }
            }
        });

        // Next file btn
        m_next_btn = ( ImageButton ) findViewById( R.id.next_btn );
        m_next_btn.setOnClickListener( new View.OnClickListener()
        {
            public void onClick( View arg0 )
            {
                try
                {
                    controller.command( "next" );
                }
                catch( Exception e )
                {
                    Toast.makeText( MoteActivity.this, R.string.commanderror, Toast.LENGTH_SHORT ).show();
                }
            }
        });

        m_serv_btn = ( ImageButton )findViewById( R.id.serv_btn );
        m_serv_btn.setOnClickListener( new View.OnClickListener()
        {
            public void onClick( View v )
            {
                int popHeight = 200; // WindowManager.LayoutParams.WRAP_CONTENT
                int popWidth = 180; // WindowManager.LayoutParams.WRAP_CONTENT
                int popMargin = 0;

                PopupDlg p = new PopupDlg( MoteActivity.this, R.layout.popup, popWidth, popHeight )
                {
                    @Override
                    public void getView( View container )
                    {
                        ListView lv = ( ListView )container.findViewById( R.id.serv_list );
                        lv.setAdapter( m_servListAdapter );

                        // manage IP click, select server IP
                        lv.setOnItemClickListener( new OnItemClickListener()
                        {
                            public void onItemClick( AdapterView<?> arg0, View v, int position, long arg3 )
                            {
                                controller = m_servListAdapter.getItem( position );
                                dismiss();
                            }
                        });

                        // manage IP long click, TODO popup menu with [edit, delete]
                        lv.setOnItemLongClickListener( new AdapterView.OnItemLongClickListener()
                        {
                            public boolean onItemLongClick( AdapterView<?> arg0, View arg1, int position, long arg3 )
                            {
                                VLCController tmp = m_servListAdapter.getItem( position );
                                String key = tmp.getIp( true );
                                m_servListAdapter.remove( tmp );

                                SharedPreferences.Editor prefs = getSharedPreferences( MoteActivity.IP_CONF_FILE, 0 ).edit();
                                prefs.remove( key ).commit();

                                return true;
                            }
                        });

                        // Manage add server button
                        Button add = ( Button )container.findViewById( R.id.serv_add_btn );
                        add.setOnClickListener( new View.OnClickListener()
                        {
                            public void onClick( View arg0 )
                            {
                                dismiss();

                                PopupDlg addServPopup = new PopupDlg(  MoteActivity.this, R.layout.addserver, 250, WindowManager.LayoutParams.WRAP_CONTENT )
                                {
                                    @Override
                                    public void getView( View container )
                                    {
                                        final EditText iptxt = ( EditText )container.findViewById( R.id.serverip_txt );
                                        final EditText porttxt = ( EditText )container.findViewById( R.id.serverport_txt );

                                        Button ok = ( Button )container.findViewById( R.id.addserver_btn );
                                        ok.setOnClickListener( new View.OnClickListener()
                                        {
                                            public void onClick( View arg0 )
                                            {
                                                String ip = iptxt.getText().toString();
                                                int port = Integer.valueOf( porttxt.getText().toString() );
                                                m_servListAdapter.add( new VLCController( ip, port ) );

                                                SharedPreferences.Editor prefs = getSharedPreferences( MoteActivity.IP_CONF_FILE, 0 ).edit();
                                                prefs.putString( ip + ":" + port, "");
                                                prefs.commit();

                                                dismiss();
                                            }
                                        });
                                    }
                                };
                                addServPopup.showAtLocation( addServPopup.getContentView(), Gravity.CENTER, 0, 0 );
                            }
                        }); // End add server button
                    }
                };

                p.setAnimationStyle( R.style.Animations_PopUpMenu_Left );
                p.showAsDropDown( m_serv_btn, popMargin, -m_serv_btn.getMeasuredHeight() - popHeight - popMargin );
            }
        } );

        m_browse_btn = ( ImageButton )findViewById( R.id.browse_btn );
        m_browse_btn.setOnClickListener( new View.OnClickListener()
        {
            public void onClick( View v )
            {
                startActivity( new Intent( getApplicationContext(), BrowseActivity.class ) );
            }
        });

        m_prefs_btn = ( ImageButton )findViewById( R.id.prefs_btn );
        m_prefs_btn.setOnClickListener( new View.OnClickListener()
        {
            public void onClick( View v )
            {
                startActivity( new Intent( getApplicationContext(), SettingsActivity.class ) );
            }
        } );

        m_help_btn = ( ImageButton )findViewById( R.id.help_btn );

        // Seek bar
        m_title_txt = ( TextView ) findViewById( R.id.title_txt );
        m_seekpos_txt = ( TextView ) findViewById( R.id.seekpos_txt );
        m_seeklen_txt = ( TextView ) findViewById( R.id.seeklen_txt );
        m_seek_sb = ( SeekBar ) findViewById( R.id.length_bar );
        m_seek_sb.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener()
        {
            public void onProgressChanged( SeekBar obj, int progress, boolean arg2 )
            {
                // real time update for current seeking position
                try
                {
                    Integer possecs = controller.seekLen.get() * progress / 100;
                    Duration tmp = new Duration( possecs );
                    m_seekpos_txt.setText( tmp.toString() );
                }
                catch(Exception e)
                {
                }
            }

            public void onStartTrackingTouch( SeekBar arg0 )
            {
                m_seek_lock = true;
            }

            public void onStopTrackingTouch( SeekBar arg0 )
            {
                m_seek_lock = false;
                try
                {
                    Integer seekcurrent = m_seek_sb.getProgress();
                    controller.command( "seek", new Object[] { seekcurrent } );
                }
                catch( Exception e )
                {
                    Toast.makeText( MoteActivity.this, R.string.commanderror, Toast.LENGTH_SHORT ).show();
                }
            }
        });
    }

    private void updateUiSeekbar()
    {
        if( ! m_seek_lock )
        {
            m_seek_sb.setProgress( controller.getSeekPosPercent() );
            m_seeklen_txt.setText( controller.seekLen.toString() );
            m_seekpos_txt.setText( controller.seekPos.toString() );
        }
    }

    /**
     * handler used to update play btn
     */
    final private Handler m_updatePlayBtnHdl = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            updatePlayBtn();
        }
    };
    private void updatePlayBtn()
    {
        if( VLCController.STATUS.PLAYING == controller.status )
        {
            m_play_btn.setImageResource( R.drawable.pause_w );
        }
        else
        {
            m_play_btn.setImageResource( R.drawable.play_w );
        }
    }

    protected void startUpdateTimer()
    {
        startUpdateTimer( 1000 );
    }
    synchronized protected void startUpdateTimer( int delay )
    {
        m_timerHdl.postDelayed( m_timerUpd, delay );
    }
    synchronized protected void stopUpdateTimer()
    {
        m_timerHdl.removeCallbacksAndMessages( null );
    }
}
