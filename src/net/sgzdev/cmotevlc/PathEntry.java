package net.sgzdev.cmotevlc;

import java.util.EnumMap;
import java.util.Map;

public class PathEntry
{
    public static enum TYPE { DIR, FILE };

    final private static Map s_type = new EnumMap<TYPE, String>(TYPE.class);
    static
    {
        s_type.put( TYPE.DIR, "directory" );
        s_type.put( TYPE.FILE, "file" );
    }

    private TYPE m_type;
    private String m_name;
    private String m_url;

    public PathEntry( TYPE t, String name, String url )
    {
        m_type = t;
        m_name = name;
        m_url = url;
    }

    /**
     * getters
     */
    public TYPE getType()
    {
        return m_type;
    }
    public String getTypeStr()
    {
        return (String)s_type.get( m_type );
    }
    public String getName()
    {
        return m_name;
    }
    public String getUrl()
    {
        return m_url;
    }

    @Override
    public String toString()
    {
        return getTypeStr() + " : " + getName() + " @ " + getUrl();
    }
}
