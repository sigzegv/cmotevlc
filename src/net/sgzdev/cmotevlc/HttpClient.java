package net.sgzdev.cmotevlc;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class HttpClient extends DefaultHttpClient
{
    public static enum QUERY_TYPE { GET, POST };

    HttpParams param = null;
    private HttpResponse m_response = null;

    public HttpClient()
    {
        reset();
    }

    final public void reset()
    {
        param = new BasicHttpParams();
    }

    public void setTimeOut( int v )
    {
        HttpConnectionParams.setConnectionTimeout( param, 3000 );
    }

    public int getResponseCode()
    {
        if(null == m_response)
        {
            return 0;
        }

        return m_response.getStatusLine().getStatusCode();
    }

    public String getResponsePage()
    {
        return getResponsePage( m_response );
    }

    /**
     * Downloads an HTTP response page
     * @return String Downloaded page
     */
    public static String getResponsePage( HttpResponse resp )
    {
        StringBuilder page = new StringBuilder("");

        try
        {
            InputStreamReader is = new InputStreamReader( resp.getEntity().getContent() );
            BufferedReader br = new BufferedReader( is );

            String line = "";
            while( ( line = br.readLine() ) != null )
            {
                page.append( line );
                page.append( System.getProperty( "line.separator" ) );
            }
            br.close();
            is.close();
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }

        return page.toString();
    }

    public void query( String url ) throws Exception
    {
        prepareQuery();

        m_response = execute( new HttpGet( url ) );
    }

    public void queryPost( String url, List<NameValuePair> params )  throws Exception
    {
        prepareQuery();

        HttpPost httppost = new HttpPost( url );
        httppost.setEntity( new UrlEncodedFormEntity( params ) );

        m_response = execute( httppost );
    }

    private void prepareQuery()
    {
        if( null != m_response )
        {
            HttpEntity entity = m_response.getEntity();

            try
            {
                entity.consumeContent();
            }
            catch( Exception e )
            {
                e.printStackTrace();
            }

        }

        setParams( param );
    }
}
