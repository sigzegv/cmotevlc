package net.sgzdev.cmotevlc;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


/**
 * File browser
 */
public class BrowseActivity extends ListActivity
{
    private final static int REFRESH_THREAD_COMPLETE = 1;
    private final int REFRESH_DIALOG = 1;

    private BrowseAdapter adapter;
    private ProgressDialog m_refreshDlg = null;
    private RefreshThread m_refreshthread = null;
    private static String currentPath = "";


    @Override
    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.browser);
        setResult(RESULT_CANCELED);

        adapter = new BrowseAdapter(this, R.layout.browser_row);
        setListAdapter(adapter);

        ListView lv = getListView();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                PathEntry e = adapter.data.get( position );
                Log.d( "BrowseActivity::ListView::onItemClick", e.toString() );

                if( PathEntry.TYPE.DIR == e.getType() )
                {
                    currentPath = e.getUrl();
                    BrowseActivity.this.showDialog( BrowseActivity.this.REFRESH_DIALOG ); // refresh file list
                }
                else
                {
                    try
                    {
                        MoteActivity.controller. run( e.getUrl() );
                        MoteActivity.controller.status = VLCController.STATUS.PLAYING;
                    }
                    catch(Exception ex)
                    {
                        ex.printStackTrace();
                    }

                    setResult( RESULT_OK );
                    finish();
                }
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        showDialog( REFRESH_DIALOG );
    }

    @Override
    protected Dialog onCreateDialog(int id)
    {
        switch( id )
        {
            case REFRESH_DIALOG:
                m_refreshDlg = new ProgressDialog( this );
                m_refreshDlg.setMessage( "Refreshing..." );
                return m_refreshDlg;

            default:
                return null;
        }
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog)
    {
        switch(id)
        {
            case REFRESH_DIALOG:
                m_refreshthread = new RefreshThread( m_handler );
                m_refreshthread.start();
        }
    }


    /**
     * Update browser's title
     */
    private void updateTitle()
    {
        String title = "/";
        if( !"".equals( currentPath ) )
        {
            title = currentPath;
        }

        TextView curdir_txt = ( TextView )findViewById( R.id.curdir_txt );
        curdir_txt.setText( title );
    }

    /**
     * data apdater
     */
    public class BrowseAdapter extends ArrayAdapter<PathEntry>
    {
        public List<PathEntry> data;

        private int m_layout;
        private Context m_context;

        /**
         * ctor
         */
        public BrowseAdapter( Context context, int layout )
        {
            this( context, layout, new ArrayList<PathEntry>());
        }
        public BrowseAdapter(Context ctx, int layout, List<PathEntry> d)
        {
            super(ctx, layout, d);

            data = d;
            m_layout = layout;
            m_context = ctx;
        }

        /**
         * display data
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if(convertView == null)
            {
                convertView = View.inflate( m_context, m_layout, null );
            }
            View v = convertView;

            PathEntry e = data.get(position);
            TextView txt = ( TextView )v.findViewById(R.id.name_txt);
            txt.setText( e.getName() );

            ImageView i = (ImageView)v.findViewById(R.id.type_img);
            PathEntry.TYPE t = e.getType();

            if( PathEntry.TYPE.DIR == t )
            {
                i.setImageResource(R.drawable.folder_w);
            }
            else
            {
                i.setImageResource(R.drawable.computer_w);
            }

            return v;
        }

    } // end class BrowserAdapter


    final private Handler m_handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            if( REFRESH_THREAD_COMPLETE == msg.arg1 )
            {
                BrowseActivity.this.m_refreshDlg.dismiss();
                BrowseActivity.this.updateTitle();
                BrowseActivity.this.adapter.notifyDataSetChanged();
            }
        }
    };

    /**
     * refresh thread
     */
    class RefreshThread extends Thread
    {
        private Handler m_handler;
        private boolean m_running = false;

        public RefreshThread(Handler h)
        {
            m_handler = h;
        }

        public void setRunning(boolean run)
        {
            m_running = run;
        }

        public boolean isRunning()
        {
            return m_running;
        }

        @Override
        public void run()
        {
            setRunning( true );

            // do job
            try
            {
                adapter.data.clear();

                List<PathEntry> files = MoteActivity.controller.browse(currentPath);
                if(0 == files.size() && "".equals( currentPath ) )
                {
                    currentPath = "/";
                    files = MoteActivity.controller.browse(currentPath); // try to load from unix FS
                }

                adapter.data.addAll( files );
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }

            setRunning( false );

            Message msg = m_handler.obtainMessage();
            msg.arg1 = REFRESH_THREAD_COMPLETE;
            m_handler.sendMessage( msg );

        } // End run()
    } // end class RefreshThread
}
