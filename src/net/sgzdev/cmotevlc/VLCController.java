package net.sgzdev.cmotevlc;

import java.io.StringReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class VLCController
{
    final public static Object lock = new Object();

    /**
     * Remote player status :
     *
     * NONE    no status, offline, or disconnected
     * PLAYING playing file
     * PAUSED  paused
     * STOPPED online and not playing file
     */
    public static enum STATUS {
        NONE,
        PLAYING,
        PAUSED,
        STOPPED
    };

    public int apiversion = 0;
    public String version = "";
    public STATUS status = STATUS.NONE;
    public String title = "";
    public int volume;
    public Duration seekLen = new Duration();
    public Duration seekPos = new Duration();

    protected String m_ip;
    protected int m_port;

    private static int MAX_VOLUME = 512;
    private final Set<String> m_queryTags = new HashSet<String>(
        Arrays.asList( new String[] { "state", "title", "volume", "length", "time", "version", "apiversion", "info" }
    ));

    public VLCController()
    {
    }

    public VLCController( String ip )
    {
        set( ip, 8080 );
    }

    public VLCController( String ip, int port )
    {
        set( ip, port );
    }

    public String getIp()
    {
        return getIp( false );
    }
    public String getIp( boolean addPort )
    {
        return ( addPort ? m_ip + ":" + getPort() : m_ip );
    }
    public int getPort()
    {
        return m_port;
    }

    final public void set( String ip, int port )
    {
        setIp( ip );
        setPort( port );
    }
    final public void setIp( String ip )
    {
        m_ip = ip;
    }
    final public void setPort( int p )
    {
        m_port = p;
    }

    /**
     * Toggle play/pause
     * @throws Exception
     */
    public void play() throws Exception
    {
        callUrl( "http://" + getIp( true ) + "/requests/status.xml?command=pl_pause" );
    }


    public void run( String filename ) throws Exception
    {
        filename = filename.replaceAll( "\\s", "%20");

        if(filename.contains(":/")) // are we browsing windows files ?
        {
            filename = filename.replaceAll( "/", "%5C");
        }

        String url = "http://" + getIp( true ) + "/requests/status.xml?command=in_play&input=" + filename;
        Log.d( "VLCController::run", url );

        callUrl( url );
    }

    /**
     * Toggle fullscreen mode
     * @throws Exception
     */
    public void fullscreen() throws Exception
    {
        callUrl( "http://" + getIp( true ) + "/requests/status.xml?command=fullscreen" );
    }

    /**
     * Set volume on player
     * @param v
     * @throws Exception
     */
    public void volume( Integer v ) throws Exception
    {
        if( 0 > v )
        {
            v = 0;
        }

        if( MAX_VOLUME <= v )
        {
            v = MAX_VOLUME;
        }

        callUrl( "http://" + getIp( true ) + "/requests/status.xml?command=volume&val=" + v );
    }
    public void volume( String v ) throws Exception
    {
        callUrl( "http://" + getIp( true ) + "/requests/status.xml?command=volume&val=" + v );
    }

    public int getVolumePercent()
    {
        return volume * 100 / MAX_VOLUME;
    }
    public int getVolumeFromPercent( int v )
    {
        return ( v * MAX_VOLUME / 100 ) + 1; // used to round correctly converted value
    }

    /**
     * seeking position from total length
     * @return int position in %
     */
    public int getSeekPosPercent()
    {
        int timeLen = seekLen.get();//seekLen.getTime();
        if( 0 == timeLen )
        {
            return 0;
        }

        return seekPos.get() * 100 / timeLen;
    }

    /**
     * Get filename from filepath. Usefull when player returns complete filepath for current file
     * @return String filename without path
     */
    public String getFilename()
    {
        String tmptitle = title.replaceAll( "\\\\", "/" );
        String [] s = tmptitle.split( "/" );
        return ( 0 == s.length ? title : s[s.length - 1] );
    }

    /**
     * Set seek position on current file
     *
     * @param v
     * @throws Exception
     */
    public void seek( Integer v ) throws Exception
    {
        callUrl( "http://" + getIp( true ) + "/requests/status.xml?command=seek&val=" + v + "%25" );
    }

    public void previous() throws Exception
    {
        callUrl( "http://" + getIp( true ) + "/requests/status.xml?command=pl_previous" );
    }

    public void rewind() throws Exception
    {
        throw new Exception("rewind not implemented");
    }

    public void forward() throws Exception
    {
        throw new Exception("forward not implemented");
    }

    public void next() throws Exception
    {
        callUrl( "http://" + getIp( true ) + "/requests/status.xml?command=pl_next" );
    }

    /**
     * Refresh status
     * @throws Exception
     */
    public void update() throws Exception
    {
        String page = callUrl( "http://" + getIp( true ) + "/requests/status.xml" );

        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware( true );
        XmlPullParser xpp = factory.newPullParser();

        xpp.setInput( new StringReader( page ) );
        int eventType = xpp.getEventType();

        String lastTag = "";
        while( XmlPullParser.END_DOCUMENT != eventType )
        {
            // search for tags
            if( XmlPullParser.START_TAG == eventType )
            {
                if( m_queryTags.contains( xpp.getName() ) )
                {
                    lastTag = xpp.getName();
                }
            }

            // get tags content
            if( XmlPullParser.TEXT == eventType )
            {
                if( m_queryTags.contains( lastTag ) )
                {
                    setTagData( lastTag, xpp.getText() );
                }
                lastTag = "";
            }
            eventType = xpp.next();
        }
    }

    /**
     * Retrieves data from selected tag
     * @param tag
     * @param data
     */
    private void setTagData( String tag, String data )
    {
        if( "".equals( tag ) )
        {
            return;
        }

        try
        {
            this.getClass().getDeclaredMethod( "parse_" + tag, data.getClass() ).invoke( this, data );
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    protected void parse_state( String data )
    {
        if( "paused".equals( data ) )
        {
            status = STATUS.PAUSED;
        }

        if( "playing".equals( data ) )
        {
            status = STATUS.PLAYING;
        }

        if( "stopped".equals( data ) || // vlc v2
            "stop".equals( data ) ) // vlc v1
        {
            status = STATUS.STOPPED;
        }
    }
    protected void parse_title( String data )
    {
        title = data;
    }
    protected void parse_volume( String data )
    {
        volume = Integer.valueOf( data );
    }
    protected void parse_length( String data )
    {
        seekLen.set( Integer.valueOf( data ) );
    }
    protected void parse_time( String data )
    {
        seekPos.set(  Integer.valueOf( data ) );
    }
    protected void parse_version( String data )
    {
        version = data;
    }
    protected void parse_apiversion( String data )
    {
        apiversion = Integer.valueOf( data );
    }

    protected void parse_info( String data )
    {

    }


    /**
     * Browse file on VLC
     *
     * @param path directory to  browse
     *
     * @return
     */
    public List<PathEntry> browse(String path) throws Exception
    {
        // query player
        path = URLEncoder.encode(path, "UTF-8");
        path = path.replaceAll("[+]", "%20"); // VLC does not support '+' as space char

        String url = "http://" + getIp(true) + "/requests/browse.xml?dir=" + path;

        String content = callUrl(url);

        // init xml parser
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();

        xpp.setInput( new StringReader(content));
        int eventType = xpp.getEventType();

        List<PathEntry> files = new ArrayList<PathEntry>();

        // start parsing
        while(XmlPullParser.END_DOCUMENT != eventType)
        {
            if(XmlPullParser.START_TAG == eventType)
            {
                if("element".equals( xpp.getName()))
                {
                    String type = xpp.getAttributeValue( null, "type" );
                    String name = xpp.getAttributeValue( null, "name" );
                    String filepath = xpp.getAttributeValue( null, "path" );
                    String tmpName = name.toLowerCase();

                    // filter on some sys dirs
                    if(!"recycler".equals(tmpName)
                        && !"$recycle.bin".equals(tmpName)
                        && !"system volume information".equals(tmpName)
                        && !"lost+found".equals(tmpName))
                    {
                        // normalize slashes
                        filepath = filepath.replaceAll( "\\\\", "/" );
                        filepath = filepath.replaceAll( "//", "/" );

                        // rebuild path for directory '..'
                        if("..".equals(name))
                        {
                            String[] c = filepath.split("/");

                            if(c.length <= 2)
                            {
                                filepath = "/";
                            }
                            else
                            {
                                filepath = "";
                                for(int i = 0; i < c.length - 2; i++)
                                {
                                    filepath += c[i] + "/";
                                }
                            }
                        }

                        files.add(new PathEntry(type.contains("dir") ? PathEntry.TYPE.DIR : PathEntry.TYPE.FILE, name, filepath));
                    }
                }
            }

            eventType = xpp.next();
        }

        Collections.sort( files, new PathEntrySorter());
        return files;
    }


    /**
     * Query URL to server
     *
     * @param url        query
     *
     * @return           response page
     *
     * @throws Exception
     */
    private String callUrl( String url ) throws Exception
    {
        HttpClient cli = new HttpClient();
        cli.setTimeOut( 2000 );
        cli.query( url );
        return cli.getResponsePage();
    }

    /**
     * Send command to server
     *
     * @param cmd method to call on player object
     */
    public void command( String cmd )
    {
        CommandThread r = new CommandThread(this, cmd);
        r.start();
    }
    public void command( String cmd, Handler h )
    {
        CommandThread r = new CommandThread(this, cmd, h);
        r.start();
    }
    public void command( String cmd, Object [] data )
    {
        CommandThread r = new CommandThread( this, cmd, data );
        r.start();
    }
    public void command( String cmd, Object [] data, Handler h )
    {
        CommandThread r = new CommandThread( this, cmd, data, h );
        r.start();
    }

    /**
     * Send asynchronous commands to remote player
     */
    public static class CommandThread extends Thread
    {
        private String m_cmd = "";
        private VLCController m_player;
        private Object [] m_data = null;
        private Handler m_CallbackHandler = null;

        public CommandThread(VLCController o, String cmd)
        {
            m_cmd = cmd;
            m_player = o;
        }
        public CommandThread(VLCController o, String cmd, Handler h)
        {
            this(o, cmd);
            m_CallbackHandler = h;
        }
        public CommandThread(VLCController o, String cmd, Object [] data)
        {
            this(o, cmd);
            m_data = data;
        }
        public CommandThread(VLCController o, String cmd, Object [] data, Handler h)
        {
            this(o, cmd, data);
            m_CallbackHandler = h;
        }

        @Override
        public void run()
        {
            int returnValue = 0;
            try
            {
                //Log.d( "CommandThread::run", "Running command '" + m_cmd + "'" );
                int len = (m_data == null ? 0 : m_data.length);

                Class<?> param_types[] = new Class<?>[len];
                for(int i = 0; i < len; i++)
                {
                    param_types[i] = m_data[i].getClass();
                }

                synchronized(VLCController.lock)
                {
                    m_player.getClass().getMethod(m_cmd, param_types ).invoke( m_player, m_data);
                }
            }
            catch(Exception e)
            {
                returnValue = 1;
            }

            if(null != m_CallbackHandler)
            {
                Message msg = m_CallbackHandler.obtainMessage();
                msg.arg1 = returnValue;
                m_CallbackHandler.sendMessage(msg);
            }
        }
    } // end class CommandThread


    /**
     * Sort files on browser
     */
    class PathEntrySorter implements Comparator<PathEntry>
    {
        public int compare(PathEntry s1, PathEntry s2)
        {
            // sort directories then files
            if(s1.getType() != s2.getType())
            {
                if(PathEntry.TYPE.DIR == s1.getType())
                {
                    return -1;
                }

                return 1;
            }

            return 0;
        }
    }
}
