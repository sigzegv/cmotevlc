package net.sgzdev.cmotevlc;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.widget.Toast;

public class App extends Application
{
    public static Context context;

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        App.context = this.getApplicationContext();
    }

    @Override
    public void onLowMemory()
    {
        super.onLowMemory();
        Toast.makeText(context, "Low Memory detected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTerminate()
    {
        super.onTerminate();
    }
}

