package net.sgzdev.cmotevlc;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

/**
 *
 * @author carlos
 */
abstract public class PopupDlg extends PopupWindow
{
    private View m_container;

    private View.OnTouchListener m_defaultTouchListener = new View.OnTouchListener() {

        public boolean onTouch( View v, MotionEvent event )
        {
            if( event.getAction() == MotionEvent.ACTION_OUTSIDE )
            {
                dismiss();
                return true;
            }
            return false;
        }
    };

    public PopupDlg( Context c, int resLayout, int width, int height )
    {
        super( c );

        m_container = View.inflate( c, resLayout, null);

        setWidth( width );
        setHeight( height );
        setContentView( m_container );
        setFocusable( true );
        setBackgroundDrawable( new BitmapDrawable() );
        setTouchInterceptor( m_defaultTouchListener );

        getView( m_container );
    }

    public PopupDlg( Context c, int resLayout )
    {
        this( c, resLayout, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT );
    }

    abstract public void getView( View container );
}
