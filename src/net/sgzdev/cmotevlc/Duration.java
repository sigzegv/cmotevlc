package net.sgzdev.cmotevlc;

import android.util.Log;

public final class Duration
{
    private int m_seconds;

    public Duration()
    {
    }
    public Duration( String t )
    {
        set( t );
    }
    public Duration( int secs )
    {
        set( secs );
    }

    /**
     * Converts time string to seconds, should be formated like h:m:s or h:m
     *
     * @param t string containing time value
     */
    public void set( String t )
    {
        String time[] = t.split( ":" );

        int secs = 0;
        if( time.length == 3 )
        {
            secs = Integer.valueOf( time[2] ); // add seconds
        }

        set( Integer.valueOf( time[0] ), Integer.valueOf( time[1] ), secs );
    }

    /**
     * Sets this duration to passed seconds
     *
     * @param secs
     */
    public void set( int secs )
    {
        m_seconds = secs;
    }
    public void set( int hours, int minutes )
    {
        set( hours, minutes, 0 );
    }
    public void set( int hours, int minutes, int seconds )
    {
        m_seconds = seconds;
        m_seconds += minutes * 60;
        m_seconds += hours * 3600;
    }

    /**
     * Return duration in seconds
     * @return
     */
    public int get()
    {
        return m_seconds;
    }

    /**
     * returns visual string from duration
     */
    @Override
    public String toString()
    {
        float tmp =  m_seconds / 3600.0f;
        int hours = (int)tmp;

        tmp -= hours;
        tmp = tmp * 60;
        int minutes = (int)tmp;

        tmp -= minutes;
        tmp = tmp * 60;
        int secs = (int)tmp;

        return String.format("%02d:%02d:%02d", hours, minutes, secs);
    }
}
