## CMoteVLC for Android
### What is This

An Android remote controller for VLC player.

---
### Project Files Notes

#### Sublime Text 2

To enable linting on Sublime Text 2 with ``SublimeLinter`` plugin, add this configuration to your project's settings,
replacing *PATH_TO_YOUR_PROJECT* and *PATH_TO_ANDROID_SDK* with your own paths, and *android-XX* with right version.

    "settings": {
        "SublimeLinter":
        {
            "Java":
            {
                "working_directory": "PATH_TO_YOUR_PROJECT/",
                "lint_args":
                [
                    "-sourcepath", "src:gen",
                    "-classpath", "PATH_TO_ANDROID_SDK/platforms/android-XX/android.jar",
                    "-Xlint", "-Xlint:empty,-serial,-unchecked",
                    "{filename}"
                ]
            }
        },

